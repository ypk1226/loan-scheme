package com.example.learn.loan.scheme.producer;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.example.learn.loan.scheme.AbstractLoanSchemeProducer;
import com.example.learn.loan.scheme.LoanEntry;
import com.example.learn.loan.scheme.LoanScheme;
import com.example.learn.loan.scheme.consts.LoanSchemeTypeConst;

import java.util.ArrayList;
import java.util.List;

/**
 * 一次还本付息
 * <p>
 * 这种还款方式，操作很简单，但是，适应的人群面比较窄，必须注意的是，此方式容易使贷款人缺少还款强迫外力，造成信用损害。采用这种贷款方式，贷款人最好有较好的自我安排能力。
 * <p>
 * 到期一次还本付息额=贷款本金×[1+月利率（‰）×贷款期（月）]
 */
public class OnceCapitalInterestSchemeProducer extends AbstractLoanSchemeProducer {

    @Override
    protected Integer getLoanSchemeType() {
        return LoanSchemeTypeConst.ONCE_CAPITAL_INTEREST;
    }

    @Override
    public List<LoanScheme> produce(LoanEntry loanEntry) {
        BigDecimal monthRate = super.getMonthRate(loanEntry.getProfitRate());
        LoanScheme loanScheme = new LoanScheme();
        loanScheme.setSumPeriod(1);
        loanScheme.setCurrPeriod(1);
        loanScheme.setCapitalAmount(loanEntry.getAmount());
        loanScheme.setFeeAmount(new BigDecimal(loanEntry.getAmount())
                .multiply(monthRate.multiply(new BigDecimal(loanEntry.getPeriods())))
                .setScale(0, BigDecimal.ROUND_HALF_UP).longValue());
        loanScheme.setAgreedDate(LocalDate.now());
        loanScheme.setTotalAmount(loanScheme.getCapitalAmount() + loanScheme.getFeeAmount());

        List<LoanScheme> result = new ArrayList<>(1);
        result.add(loanScheme);
        return result;
    }
}
