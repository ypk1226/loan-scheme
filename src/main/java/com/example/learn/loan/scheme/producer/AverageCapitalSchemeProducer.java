package com.example.learn.loan.scheme.producer;

import com.example.learn.loan.scheme.AbstractLoanSchemeProducer;
import com.example.learn.loan.scheme.LoanEntry;
import com.example.learn.loan.scheme.LoanScheme;
import com.example.learn.loan.scheme.consts.LoanSchemeTypeConst;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * 等额本金，又称利随本清、等本不等息还款法。贷款人将本金分摊到每个月内，同时付清上一交易日至本次还款日之间的利息。
 * <p>
 * 这种还款方式相对等额本息而言，总的利息支出较低，但是前期支付的本金和利息较多，还款负担逐月递减。
 * <p>
 * 计算公式：
 * <p>
 * 每月还本付息金额=(本金/还款月数)+(本金-累计已还本金)×月利率
 * <p>
 * 每月本金=总本金/还款月数
 * <p>
 * 每月利息=(本金-累计已还本金)×月利率
 * <p>
 * 还款总利息=（还款月数+1）*贷款额*月利率/2
 * <p>
 * 还款总额=(还款月数+1)*贷款额*月利率/2+贷款额
 */
public class AverageCapitalSchemeProducer extends AbstractLoanSchemeProducer {
    @Override
    protected Integer getLoanSchemeType() {
        return LoanSchemeTypeConst.AVERAGE_CAPITAL;
    }

    @Override
    public List<LoanScheme> produce(LoanEntry loanEntry) {
        LocalDate today = LocalDate.now();
        //计算每一期金额
        long amount = new BigDecimal(loanEntry.getAmount()).divide(new BigDecimal(loanEntry.getPeriods()), 0, BigDecimal.ROUND_HALF_UP).longValue();

        List<LoanScheme> result = new ArrayList<>(loanEntry.getPeriods());
        Long sumAmount = 0L;
        for (int i = 1; i <= loanEntry.getPeriods(); i++) {
            LoanScheme loanScheme = new LoanScheme();
            loanScheme.setSumPeriod(i);
            loanScheme.setCurrPeriod(loanEntry.getPeriods());
            loanScheme.setAgreedDate(today.plusMonths(i - 1));

            loanScheme.setFeeAmount(new BigDecimal(loanEntry.getAmount() - sumAmount)
                    .multiply(super.getMonthRate(loanEntry.getProfitRate()))
                    .setScale(0, BigDecimal.ROUND_HALF_UP).longValue());

            if (i < loanEntry.getPeriods()) {
                loanScheme.setCapitalAmount(amount);
            } else {
                loanScheme.setCapitalAmount(loanEntry.getAmount() - sumAmount);//最后一笔，使用减法计算
            }
            loanScheme.setTotalAmount(loanScheme.getCapitalAmount() + loanScheme.getFeeAmount());

            sumAmount += amount;
            result.add(loanScheme);
        }
        return result;
    }

}
