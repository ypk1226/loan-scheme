package com.example.learn.loan.scheme;

import java.math.BigDecimal;

/**
 * 融资信息
 */
public class LoanEntry {
    private Long amount;//金额
    private Integer periods;//分期期数
    private BigDecimal profitRate;//费率

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Integer getPeriods() {
        return periods;
    }

    public void setPeriods(Integer periods) {
        this.periods = periods;
    }

    public BigDecimal getProfitRate() {
        return profitRate;
    }

    public void setProfitRate(BigDecimal profitRate) {
        this.profitRate = profitRate;
    }

}
