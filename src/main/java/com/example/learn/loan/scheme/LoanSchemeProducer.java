package com.example.learn.loan.scheme;

import java.util.List;

public interface LoanSchemeProducer {
    List<LoanScheme> produce(LoanEntry loanEntry);
}
