package com.example.learn.loan.scheme;

import java.util.List;

public class LoanSchemesPrinter {

    public static void print(List<LoanScheme> loanSchemes, LoanEntry loanEntry) {
        for (LoanScheme loanScheme : loanSchemes) {
            System.out.println(loanScheme);
        }

        System.out.println("total=" + loanSchemes.stream().mapToLong(LoanScheme::getTotalAmount).sum());
        System.out.println("capital=" + loanSchemes.stream().mapToLong(LoanScheme::getCapitalAmount).sum());
        System.out.println("fee=" + loanSchemes.stream().mapToLong(LoanScheme::getFeeAmount).sum());
    }
}
