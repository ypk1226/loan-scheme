package com.example.learn.loan.scheme.producer;

import com.example.learn.loan.scheme.AbstractLoanSchemeProducer;
import com.example.learn.loan.scheme.LoanEntry;
import com.example.learn.loan.scheme.LoanScheme;
import com.example.learn.loan.scheme.consts.LoanSchemeTypeConst;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * 信用卡分期，这种分期方式的利息，是按照一次还本付息的方式计算的，不考虑中间已还资金对利息的影响，对贷款者最不利。
 * <p>
 * 利息 = 总金额 * 月费率
 * 本金 = 总金额 / 期数
 */
public class CreditCardSchemeProducer extends AbstractLoanSchemeProducer {
    @Override
    protected Integer getLoanSchemeType() {
        return LoanSchemeTypeConst.CREDIT_CARD;
    }

    @Override
    public List<LoanScheme> produce(LoanEntry loanEntry) {
        LocalDate today = LocalDate.now();
        BigDecimal monthRate = super.getMonthRate(loanEntry.getProfitRate());

        //计算每一期金额
        long capitalAmount = new BigDecimal(loanEntry.getAmount()).divide(new BigDecimal(loanEntry.getPeriods()), 0, BigDecimal.ROUND_HALF_UP).longValue();

        List<LoanScheme> result = new ArrayList<>(loanEntry.getPeriods());
        Long sumAmount = 0L;
        for (int i = 1; i <= loanEntry.getPeriods(); i++) {
            LoanScheme loanScheme = new LoanScheme();
            loanScheme.setSumPeriod(i);
            loanScheme.setCurrPeriod(loanEntry.getPeriods());
            loanScheme.setAgreedDate(today.plusMonths(i - 1));

            loanScheme.setFeeAmount(new BigDecimal(loanEntry.getAmount())
                    .multiply(super.getMonthRate(loanEntry.getProfitRate()))
                    .setScale(0, BigDecimal.ROUND_HALF_UP).longValue());

            if (i < loanEntry.getPeriods()) {
                loanScheme.setCapitalAmount(capitalAmount);
            } else {
                loanScheme.setCapitalAmount(loanEntry.getAmount() - sumAmount);//最后一笔，使用减法计算
            }
            loanScheme.setTotalAmount(loanScheme.getCapitalAmount() + loanScheme.getFeeAmount());

            sumAmount += capitalAmount;
            result.add(loanScheme);
        }
        return result;
    }
}
