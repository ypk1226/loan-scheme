package com.example.learn.loan.scheme;

import java.util.HashMap;
import java.util.Map;

public class LoanSchemeProducerFactory {
    private Map<Integer, LoanSchemeProducer> map = new HashMap<>(10);

    public void register(Integer lanSchemeType, LoanSchemeProducer producer) {
        map.put(lanSchemeType, producer);
    }

    public LoanSchemeProducer build(Integer loanSchemeType) {
        return map.get(loanSchemeType);
    }

}
