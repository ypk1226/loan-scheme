package com.example.learn.loan.scheme;

import java.time.LocalDate;
import java.util.Date;

public class LoanScheme {
    private Integer sumPeriod;//总期数
    private Integer currPeriod;//当前期数
    private Long totalAmount;//总金额
    private Long capitalAmount;//本金
    private Long feeAmount;// 手续费或服务费
    private LocalDate agreedDate;// 还款日期

    public Integer getSumPeriod() {
        return sumPeriod;
    }

    public void setSumPeriod(Integer sumPeriod) {
        this.sumPeriod = sumPeriod;
    }

    public Integer getCurrPeriod() {
        return currPeriod;
    }

    public void setCurrPeriod(Integer currPeriod) {
        this.currPeriod = currPeriod;
    }

    public Long getCapitalAmount() {
        return capitalAmount;
    }

    public void setCapitalAmount(Long capitalAmount) {
        this.capitalAmount = capitalAmount;
    }

    public Long getFeeAmount() {
        return feeAmount;
    }

    public void setFeeAmount(Long feeAmount) {
        this.feeAmount = feeAmount;
    }


    public LocalDate getAgreedDate() {
        return agreedDate;
    }

    public void setAgreedDate(LocalDate agreedDate) {
        this.agreedDate = agreedDate;
    }

    public Long getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Long totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Override
    public String toString() {
        return "LoanScheme{" +
                "sumPeriod=" + sumPeriod +
                ", currPeriod=" + currPeriod +
                ", totalAmount=" + totalAmount +
                ", capitalAmount=" + capitalAmount +
                ", feeAmount=" + feeAmount +
                ", agreedDate=" + agreedDate +
                '}';
    }
}
