package com.example.learn.loan.scheme.producer;

import com.example.learn.loan.scheme.AbstractLoanSchemeProducer;
import com.example.learn.loan.scheme.LoanEntry;
import com.example.learn.loan.scheme.LoanScheme;
import com.example.learn.loan.scheme.consts.LoanSchemeTypeConst;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 本金归还计划，是等额本金还款的变体。
 * <p>
 * 贷款人经过与银行的协商，每次本金还款不少于1万元，两次还款时间的间隔不能超过12个月，利息可以按月或是按季度归还。
 * <p>
 * 这种还款方式是银行专门为那些非月收入的入群制定的。尤其考虑到了在年底有大额奖金的人。此外，上层行的在家办公一族，很多人每个月都没有固定的收入，
 * 但是他们每完成一部作品都会有较大的收入，这些人比较适合这种还款方式。
 * <p>
 * 这里我们约定，利息按照季度还款，本金按照半年还款
 */
public class CapitalSchemeProducer extends AbstractLoanSchemeProducer {
    @Override
    protected Integer getLoanSchemeType() {
        return LoanSchemeTypeConst.CAPITAL;
    }

    @Override
    public List<LoanScheme> produce(LoanEntry loanEntry) {
        AverageCapitalSchemeProducer averageCapitalSchemeProducer = new AverageCapitalSchemeProducer();
        List<LoanScheme> loanSchemes = averageCapitalSchemeProducer.produce(loanEntry);

        List<LoanScheme> feeResult = new ArrayList<>();
        Long tempFee = 0L;
        for (int i = 0; i < loanSchemes.size(); i++) {
            tempFee += loanSchemes.get(i).getFeeAmount();
            if ((i + 1) % 3 == 0) {//每三个月合并一次利息
                LoanScheme loanScheme = createByFee(loanSchemes.get(i).getAgreedDate(), tempFee);
                feeResult.add(loanScheme);
                tempFee = 0L;
            }
        }

        if (tempFee > 0) {
            LoanScheme loanScheme = createByFee(loanSchemes.get(loanSchemes.size() - 1).getAgreedDate(), tempFee);
            feeResult.add(loanScheme);
        }

        List<LoanScheme> capitalResult = new ArrayList<>();
        Long tempCapital = 0L;
        for (int i = 0; i < loanSchemes.size(); i++) {
            tempCapital += loanSchemes.get(i).getCapitalAmount();
            if ((i + 1) % 6 == 0) {//6个月合并一次本金
                LoanScheme loanScheme = createByCapital(loanSchemes.get(i).getAgreedDate(), tempCapital);
                capitalResult.add(loanScheme);
                tempCapital = 0L;
            }
        }

        if (tempCapital > 0) {
            LoanScheme loanScheme = createByCapital(loanSchemes.get(loanSchemes.size() - 1).getAgreedDate(), tempCapital);
            capitalResult.add(loanScheme);
        }

        feeResult.addAll(capitalResult);

        //按照还款日期分组
        Map<LocalDate, List<LoanScheme>> collect = feeResult.stream().collect(Collectors.groupingBy(LoanScheme::getAgreedDate));
        List<LoanScheme> result = new ArrayList<>(collect.size());
        for (Map.Entry<LocalDate, List<LoanScheme>> entry : collect.entrySet()) {
            List<LoanScheme> value = entry.getValue();

            LoanScheme scheme = new LoanScheme();
            scheme.setAgreedDate(entry.getKey());
            scheme.setCapitalAmount(value.stream().mapToLong(LoanScheme::getCapitalAmount).sum());
            scheme.setFeeAmount(value.stream().mapToLong(LoanScheme::getFeeAmount).sum());
            scheme.setTotalAmount(value.stream().mapToLong(LoanScheme::getTotalAmount).sum());
            result.add(scheme);
        }
        result.sort(Comparator.comparing(LoanScheme::getAgreedDate));//按照日期排序

        for (int i = 0; i < result.size(); i++) {
            LoanScheme loanScheme = result.get(i);
            loanScheme.setSumPeriod(result.size());
            loanScheme.setCurrPeriod(i + 1);
        }
        return result;
    }

    private LoanScheme createByFee(LocalDate agreedDate, Long tempFee) {
        LoanScheme loanScheme = new LoanScheme();
        loanScheme.setTotalAmount(tempFee);
        loanScheme.setCapitalAmount(0L);
        loanScheme.setFeeAmount(tempFee);
        loanScheme.setAgreedDate(agreedDate);
        return loanScheme;
    }

    private LoanScheme createByCapital(LocalDate agreedDate, Long tempCapital) {
        LoanScheme loanScheme = new LoanScheme();
        loanScheme.setTotalAmount(tempCapital);
        loanScheme.setCapitalAmount(tempCapital);
        loanScheme.setFeeAmount(0L);
        loanScheme.setAgreedDate(agreedDate);
        return loanScheme;
    }
}
