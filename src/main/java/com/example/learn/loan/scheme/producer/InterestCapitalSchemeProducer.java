package com.example.learn.loan.scheme.producer;

import com.example.learn.loan.scheme.AbstractLoanSchemeProducer;
import com.example.learn.loan.scheme.LoanEntry;
import com.example.learn.loan.scheme.LoanScheme;
import com.example.learn.loan.scheme.consts.LoanSchemeTypeConst;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * 按期付息还本，是指通过和银行协商，可以制订不同还款时间单位。即自主决定按月、季度或年等时间间隔还款。
 * <p>
 * 从某种程度来说，按期付息还本是等额本息还款的变体。例如，15年期，20万元贷款，采用等额本息还款，每月还款额为1707元。如果贷款人选择比较灵活的方式，就可以选择每两个月还3414元。
 * <p>
 * 按期付息还本适用于收入不稳定人群，目前很多收入与工作量直接挂钩的年轻人有这个倾向。
 * <p>
 * 这里我们做出约定，每一期为一个季度，即三个月
 */
public class InterestCapitalSchemeProducer extends AbstractLoanSchemeProducer {
    @Override
    protected Integer getLoanSchemeType() {
        return LoanSchemeTypeConst.INTEREST_CAPITAL;
    }

    @Override
    public List<LoanScheme> produce(LoanEntry loanEntry) {
        AverageCapitalInterestSchemeProducer averageCapitalInterestSchemeProducer = new AverageCapitalInterestSchemeProducer();
        List<LoanScheme> loanSchemes = averageCapitalInterestSchemeProducer.produce(loanEntry);//按月计算等额本息

        List<LoanScheme> result = new ArrayList<>(loanSchemes.size() / 3 + 1);
        List<LoanScheme> temp = new ArrayList<>(3);
        for (int i = 0; i < loanSchemes.size(); i++) {
            temp.add(loanSchemes.get(i));
            if ((i + 1) % 3 == 0) {//每三个月合并一次
                LoanScheme loanScheme = createByTemp(loanSchemes.get(i).getAgreedDate(), temp);
                result.add(loanScheme);
                temp.clear();
            }
        }

        if (temp.size() > 0) {
            LoanScheme loanScheme = createByTemp(loanSchemes.get(loanSchemes.size() - 1).getAgreedDate(), temp);
            result.add(loanScheme);
        }

        for (int i = 0; i < result.size(); i++) {
            LoanScheme loanScheme = result.get(i);
            loanScheme.setSumPeriod(result.size());
            loanScheme.setCurrPeriod(i + 1);
        }

        return result;
    }

    private LoanScheme createByTemp(LocalDate agreedDate, List<LoanScheme> temp) {
        LoanScheme loanScheme = new LoanScheme();
        loanScheme.setTotalAmount(temp.stream().mapToLong(LoanScheme::getTotalAmount).sum());
        loanScheme.setCapitalAmount(temp.stream().mapToLong(LoanScheme::getCapitalAmount).sum());
        loanScheme.setFeeAmount(temp.stream().mapToLong(LoanScheme::getFeeAmount).sum());
        loanScheme.setAgreedDate(agreedDate);
        return loanScheme;
    }
}
