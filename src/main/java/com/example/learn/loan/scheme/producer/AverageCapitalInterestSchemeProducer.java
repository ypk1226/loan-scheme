package com.example.learn.loan.scheme.producer;

import java.time.LocalDate;

import com.example.learn.loan.scheme.AbstractLoanSchemeProducer;
import com.example.learn.loan.scheme.LoanEntry;
import com.example.learn.loan.scheme.LoanScheme;
import com.example.learn.loan.scheme.consts.LoanSchemeTypeConst;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 等额本息，每月偿还同等数额的贷款(包括本金和利息)。
 * <p>
 * 它和等额本金是不一样的概念，虽然刚开始还款时每月还款额可能会低于等额本金还款方式的额度，但是最终所还利息会高于等额本金还款方式，该方式经常被银行使用。
 * <p>
 * 每月还本付息金额 =[ 本金*月利率*(1+月利率)贷款月数 ]/[(1+月利率)还款月数 - 1]
 * <p>
 * 还款总利息=贷款额*贷款月数*月利率*(1+月利率)贷款月数/[(1+月利率)还款月数 - 1] -贷款额
 * <p>
 * 还款总额=还款月数*贷款额*月利率*(1+月利率)贷款月数/[(1+月利率)还款月数 - 1]
 * <p>
 * 每月利息 = 剩余本金*贷款月利率
 * <p>
 * 每月本金=月供-每月利息
 */
public class AverageCapitalInterestSchemeProducer extends AbstractLoanSchemeProducer {

    @Override
    protected Integer getLoanSchemeType() {
        return LoanSchemeTypeConst.AVERAGE_CAPITAL_INTEREST;
    }

    @Override
    public List<LoanScheme> produce(LoanEntry loanEntry) {
        LocalDate today = LocalDate.now();
        BigDecimal monthRate = super.getMonthRate(loanEntry.getProfitRate());
        //x = A * β * (1 + β) ^ k / [(1 + β) ^ k - 1]
        long totalAmount = new BigDecimal(loanEntry.getAmount())
                .multiply(monthRate)
                .multiply((new BigDecimal(1).add(monthRate)).pow(loanEntry.getPeriods()))
                .divide((new BigDecimal(1).add(monthRate)).pow(loanEntry.getPeriods()).subtract(new BigDecimal(1)), 0, BigDecimal.ROUND_HALF_UP).longValue();

        List<LoanScheme> result = new ArrayList<>();
        long sumCapitalAmount = 0L;
        for (int i = 1; i <= loanEntry.getPeriods(); i++) {
            LoanScheme loanScheme = new LoanScheme();
            loanScheme.setSumPeriod(i);
            loanScheme.setCurrPeriod(loanEntry.getPeriods());
            loanScheme.setAgreedDate(today.plusMonths(i - 1));

            long feeAmount = new BigDecimal(loanEntry.getAmount() - sumCapitalAmount).multiply(monthRate).setScale(0, BigDecimal.ROUND_HALF_UP).longValue();

            if (i == loanEntry.getPeriods()) {
                long capitalAmount = loanEntry.getAmount() - sumCapitalAmount;//最后一笔，使用减法计算
                loanScheme.setTotalAmount(totalAmount);
                loanScheme.setCapitalAmount(capitalAmount);
                loanScheme.setFeeAmount(totalAmount - capitalAmount);
            } else {
                loanScheme.setTotalAmount(totalAmount);
                loanScheme.setCapitalAmount(totalAmount - feeAmount);
                loanScheme.setFeeAmount(feeAmount);
            }

            result.add(loanScheme);

            sumCapitalAmount += loanScheme.getCapitalAmount();
        }

        return result;
    }
}
