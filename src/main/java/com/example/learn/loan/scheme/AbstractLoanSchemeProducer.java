package com.example.learn.loan.scheme;

import java.math.BigDecimal;
import java.math.RoundingMode;

public abstract class AbstractLoanSchemeProducer implements LoanSchemeProducer {
    protected abstract Integer getLoanSchemeType();

    protected BigDecimal getMonthRate(BigDecimal profitRate) {
        return profitRate.divide(new BigDecimal(12), 32, RoundingMode.HALF_UP);
    }
}
