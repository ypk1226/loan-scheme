package com.example.learn.loan.scheme.producer;

import com.example.learn.loan.scheme.AbstractLoanSchemeProducer;
import com.example.learn.loan.scheme.LoanScheme;
import com.example.learn.loan.scheme.LoanEntry;
import com.example.learn.loan.scheme.consts.LoanSchemeTypeConst;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.List;

/**
 * 等额递增 todo 没有公式，计算不出来
 */
public class AverageIncreaseSchemeProducer  extends AbstractLoanSchemeProducer {
    @Override
    protected Integer getLoanSchemeType() {
        return LoanSchemeTypeConst.AVERAGE_INCREASE;
    }

    @Override
    public List<LoanScheme> produce(LoanEntry loanEntry) {
//        loanEntry.setPeriods(6);
//        loanEntry.setAmount(loanEntry.getAmount() - 90);
        loanEntry.setProfitRate(new BigDecimal("0.0435"));
//        loanEntry.setProfitRate(loanEntry.getProfitRate().divide(new BigDecimal(2), 32, BigDecimal.ROUND_HALF_UP));

        BigDecimal monthRate = super.getMonthRate(loanEntry.getProfitRate());
        long totalAmount = new BigDecimal(loanEntry.getAmount())
                .multiply(monthRate)
                .multiply((new BigDecimal(1).add(monthRate)).pow(loanEntry.getPeriods()))
                .divide((new BigDecimal(1).add(monthRate)).pow(loanEntry.getPeriods()).subtract(new BigDecimal(1)), 32, BigDecimal.ROUND_HALF_UP)
                .multiply(new BigDecimal(loanEntry.getPeriods()))
                .subtract(new BigDecimal(120))
                .divide(new BigDecimal(12), 0, RoundingMode.HALF_UP).longValue();

        System.err.println(totalAmount);
        System.err.println(new BigDecimal(totalAmount * 12 - 90).divide(new BigDecimal(12), 0, RoundingMode.HALF_UP));

        AverageCapitalInterestSchemeProducer averageCapitalInterestSchemeProducer = new AverageCapitalInterestSchemeProducer();
        List<LoanScheme> produce = averageCapitalInterestSchemeProducer.produce(loanEntry);

        return null;
    }
}
