package com.example.learn.loan.scheme.producer;

import com.example.learn.loan.scheme.AbstractLoanSchemeProducer;
import com.example.learn.loan.scheme.LoanScheme;
import com.example.learn.loan.scheme.LoanEntry;
import com.example.learn.loan.scheme.consts.LoanSchemeTypeConst;

import java.util.List;

/**
 * 等额递减 todo 没有公式，计算不出来
 */
public class AverageDecreaseSchemeProducer extends AbstractLoanSchemeProducer {
    @Override
    protected Integer getLoanSchemeType() {
        return LoanSchemeTypeConst.AVERAGE_DECREASE;
    }

    @Override
    public List<LoanScheme> produce(LoanEntry loanEntry) {
        return null;
    }
}
