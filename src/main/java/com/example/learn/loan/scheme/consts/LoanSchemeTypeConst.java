package com.example.learn.loan.scheme.consts;

public interface LoanSchemeTypeConst {
    Integer AVERAGE_CAPITAL_INTEREST = 1;//等额本息
    Integer AVERAGE_CAPITAL = 2;//等额本金
    Integer ONCE_CAPITAL_INTEREST = 3;//一次还本付息
    Integer CAPITAL = 4;//本金归还计划
    Integer INTEREST_CAPITAL = 5;//按期付息还本
    Integer AVERAGE_DECREASE = 6;//等额递减
    Integer AVERAGE_INCREASE = 7;//等额递增
    Integer CREDIT_CARD = 7;//信用卡分期
}
