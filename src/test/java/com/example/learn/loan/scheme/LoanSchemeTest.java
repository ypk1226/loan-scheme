package com.example.learn.loan.scheme;

import com.example.learn.loan.scheme.consts.LoanSchemeTypeConst;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

public class LoanSchemeTest {

    @Test
    public void test() {
        LoanSchemeProducerFactory factory = ServiceInitializer.init();
        LoanEntry loanEntry = generateLoanEntry();
        testAverageCapital(LoanSchemeTypeConst.AVERAGE_CAPITAL, loanEntry, factory);//等额本金
        testAverageCapital(LoanSchemeTypeConst.AVERAGE_CAPITAL_INTEREST, loanEntry, factory);//等额本息
        testAverageCapital(LoanSchemeTypeConst.ONCE_CAPITAL_INTEREST, loanEntry, factory);//一次还本付息
        testAverageCapital(LoanSchemeTypeConst.INTEREST_CAPITAL, loanEntry, factory);//按期付息还本
        testAverageCapital(LoanSchemeTypeConst.CAPITAL, loanEntry, factory);//本金归还计划
        testAverageCapital(LoanSchemeTypeConst.CREDIT_CARD, loanEntry, factory);//信用卡分期
    }

    private LoanEntry generateLoanEntry() {
        LoanEntry loanEntry = new LoanEntry();
        loanEntry.setAmount(1000000L);
        loanEntry.setPeriods(12);
        loanEntry.setProfitRate(new BigDecimal("0.0475"));
        return loanEntry;
    }

    private void testAverageCapital(Integer loanSchemeType, LoanEntry loanEntry, LoanSchemeProducerFactory factory) {
        LoanSchemeProducer producer = factory.build(loanSchemeType);
        List<LoanScheme> loanSchemes = producer.produce(loanEntry);//执行
        LoanSchemesPrinter.print(loanSchemes, loanEntry);
    }

}

