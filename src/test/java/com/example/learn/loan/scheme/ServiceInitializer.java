package com.example.learn.loan.scheme;

import com.example.learn.loan.scheme.consts.LoanSchemeTypeConst;
import com.example.learn.loan.scheme.producer.*;

public class ServiceInitializer {

    public static LoanSchemeProducerFactory init() {
        LoanSchemeProducerFactory factory = new LoanSchemeProducerFactory();
        factory.register(LoanSchemeTypeConst.AVERAGE_CAPITAL, new AverageCapitalSchemeProducer());
        factory.register(LoanSchemeTypeConst.AVERAGE_CAPITAL_INTEREST, new AverageCapitalInterestSchemeProducer());
        factory.register(LoanSchemeTypeConst.ONCE_CAPITAL_INTEREST, new OnceCapitalInterestSchemeProducer());
        factory.register(LoanSchemeTypeConst.INTEREST_CAPITAL, new InterestCapitalSchemeProducer());
        factory.register(LoanSchemeTypeConst.CAPITAL, new CapitalSchemeProducer());
        factory.register(LoanSchemeTypeConst.AVERAGE_INCREASE, new AverageIncreaseSchemeProducer());
        factory.register(LoanSchemeTypeConst.AVERAGE_DECREASE, new AverageDecreaseSchemeProducer());
        factory.register(LoanSchemeTypeConst.CREDIT_CARD, new CreditCardSchemeProducer());
        return factory;
    }

}
